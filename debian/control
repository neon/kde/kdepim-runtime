Source: kdepim-runtime
Section: x11
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Sune Vuorela <debian@pusling.com>,
           Fathi Boudra <fabo@debian.org>,
           Modestas Vainius <modax@debian.org>,
           George Kiagiadakis <kiagiadakis.george@gmail.com>,
           Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               kf6-extra-cmake-modules,
               kf6-kcalendarcore-dev,
               kf6-kcmutils-dev,
               kf6-kcodecs-dev,
               kf6-kconfig-dev,
               kf6-kconfigwidgets-dev,
               kf6-kcontacts-dev,
               kf6-kdav-dev,
               kf6-kdoctools,
               kf6-kholidays-dev,
               kf6-kio-dev,
               kf6-kitemmodels-dev,
               kf6-knotifications-dev,
               kf6-knotifyconfig-dev,
               kf6-ktextaddons-dev,
               kf6-ktextwidgets-dev,
               kf6-kwallet-dev,
               kf6-kwindowsystem-dev,
               kf6-prison-dev,
               kf6-syndication-dev,
               kpim6-akonadi-calendar-dev,
               kpim6-akonadi-contacts-dev,
               kpim6-akonadi-dev,
               kpim6-akonadi-mime-dev,
               kpim6-akonadi-notes-dev,
               kpim6-grantleetheme-dev,
               kpim6-kcalendarutils-dev,
               kpim6-kidentitymanagement-dev,
               kpim6-kimap-dev,
               kpim6-kmailtransport-dev,
               kpim6-kmbox-dev,
               kpim6-kmime-dev,
               kpim6-kpimtextedit-dev,
               kpim6-libkdepim-dev,
               kpim6-libkgapi-dev,
               kpim6-kmailtransport-dev,
               kpim6-pimcommon-dev,
               libboost-atomic-dev,
               libboost-dev (>= 1.40.0-2),
               libboost-program-options-dev,
               libetebase-dev,
               libkolabxml-dev,
               libqca-qt6-2-dev,
               pkgconf,
               pkg-kde-tools-neon,
               qt6-base-dev,
               qt6-declarative-dev,
               qt6-networkauth-dev,
               qt6-positioning-dev,
               qt6-scxml-dev,
               qt6-speech-dev,
               qt6-webchannel-dev,
               qt6-webengine-dev,
               qtkeychain-qt6-dev,
               shared-mime-info,
               xsltproc
Standards-Version: 4.6.2
Homepage: http://pim.kde.org/
Vcs-Browser: https://anonscm.debian.org/git/pkg-kde/applications/kdepim-runtime.git
Vcs-Git: https://anonscm.debian.org/git/pkg-kde/applications/kdepim-runtime.git

Package: kpim6-kdepim-runtime
Architecture: any
Depends: kpim6-akonadi,
         kpim6-kldap,
         kpim6-libksieve,
         kpim6-libkgapi,
         ${misc:Depends},
         ${shlibs:Depends}
Replaces: kdepim-runtime
Description: runtime components for Akonadi KDE
 This package contains Akonadi agents written using KDE Development Platform
 libraries.
 Any package that uses Akonadi should probably pull this in as a dependency.
 .
 This package is part of the kdepim-runtime module.

Package: kdepim-runtime
Architecture: all
Depends: kpim6-kdepim-runtime, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.
